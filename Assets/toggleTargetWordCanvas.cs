﻿using UnityEngine;
using System.Collections;

public class toggleTargetWordCanvas : MonoBehaviour {
	private int currentLevel;
	public Canvas targetWordCanvas;
	private bool isTargetWordCanvasShowing;
	// Use this for initialization
	void Start () {
		isTargetWordCanvasShowing = false;

		currentLevel = PlayerPrefs.GetInt ("currentLevel");
		if (currentLevel == 6) {
			isTargetWordCanvasShowing = true;
		
		} else {
			isTargetWordCanvasShowing = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		targetWordCanvas.enabled = isTargetWordCanvasShowing;
	}
}
