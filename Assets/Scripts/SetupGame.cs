﻿using UnityEngine;
using System.Collections;

public class SetupGame : MonoBehaviour {

	// Use this for initialization
	void Awake ()
	{
		if (!PlayerPrefs.HasKey ("currentLevel")) {
			PlayerPrefs.SetInt ("currentLevel", 1);

		}
		if (!PlayerPrefs.HasKey ("currentSubLevel")) {
			PlayerPrefs.SetInt ("currentSubLevel", 1);

		}
		if (!PlayerPrefs.HasKey ("ClearedLevels")) {
			PlayerPrefs.SetInt ("ClearedLevels", 1);

		}
	
		PlayerPrefs.Save();
	
	}



}
