﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckClearedLevels : MonoBehaviour
{

	public int LevelOfButton;

	void Awake ()
	{
	
		setupButton ();
	}

	public void setupButton ()
	{
		int clearedLevels = PlayerPrefs.GetInt ("ClearedLevels");
		if (LevelOfButton > clearedLevels) {
			this.GetComponent<Button> ().interactable = false;
		}
	}
}
