﻿using UnityEngine;
using System.Collections;

public class InstructionMenuHandler : MonoBehaviour {

	public Canvas instructionCanvas;
	public bool isMenuShowing;

	public void Start() {

		isMenuShowing = false;
	}

	public void showMainMenu () {

		Application.LoadLevel (0);

	}

	public void resumeGame () {

		isMenuShowing = false;
	}

	void Update() {

		instructionCanvas.enabled = isMenuShowing;
	}

}
