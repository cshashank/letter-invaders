﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class spawnInvaders : MonoBehaviour
{
	public GameObject[] numberPrefabs;
	public GameObject[] letterPrefabs;
	public GameObject[] punctuationPrefabs;
	public GameObject[] symbolPrefabsFirstRow; // &, $, #, @, _
	public GameObject[] symbolPrefabsSecondRow;// ), +, =
	public GameObject[] symbolPrefabsThirdRow;// euro, (, *
	public GameObject[] symbolPrefabsFourthRow;// -, %
	public GameObject[] symbolPrefabsFifthRow;// /
	public Text targetWordText;

	public InvaderSpawnGrid[] positionMatrix;
	private Hashtable prefabLookup;
	static int currentLevel;
	static int currentSubLevel;
	private char[] characterArray;
	private string[] commonWords;

	void Awake ()
	{
		characterArray = new char[] {
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'l',
			'm',
			'n',
			'o',
			'p',
			'q',
			'r',
			's',
			't',
			'v',
			'w',
			'x',
			'y',
			'z'
		};
		GenerateInvaders ();
		commonWords = new string[] {
			"hello",
			"how",
			"is",
			"every",
			"thing",
			"going",
			"good",

		};
		// Instatntiate all the invaderClass objects by calling the appropriate method at this point.
	}

	void Start ()
	{
//		currentLevel =  GameObject.Find ("LevelManager").GetComponent<LevelManagerScript>().currentLevel;
		// Update the current level from the player prefs.
		currentLevel = PlayerPrefs.GetInt ("currentLevel");
		currentSubLevel = PlayerPrefs.GetInt ("currentSubLevel");

		if (currentLevel >= 1 && currentLevel <= 5) {

			GenerateInitialLevels (currentLevel);

		} else if (currentLevel == 6) {

			spawmInvadersForLevel6 ();

		} else if (currentLevel == 7) {
			
			spawmInvadersForLevel7 ();
		
		} else if (currentLevel == 8) {
			
			spawmInvadersForLevel8 ();

		} else if (currentLevel == 9) {
			
			spawmInvadersForLevel9 ();

		} else if (currentLevel == 10) {
			
			spawmInvadersForLevel10 ();
		} 
		else if (currentLevel == 11) {
			
			spawmInvadersForLevel11 ();

		} else {
		
			Application.LoadLevel (0);
		
		}
//		for (int i = 0; i < 3; i++) {
//
//			instantiateInvaders (1, i);
//			instantiateInvaders (2, i);
//			instantiateInvaders (3, i);
//			instantiateInvaders (4, i);
//			instantiateInvaders (5, i);
//
//		}

	}


	// Level 1 to 5.
	void GenerateInitialLevels (int currentLevel)
	{
		// The levels start from 1 according to convention. prefabs for level 1 correspond to index of 0 and so on ... 
		int invaderIndex = currentLevel - 1;

		for (int i = 0; i < 3; i++) {
			int k = invaderIndex * 5;
			for (int j = 0; j<5; j++, k++) {
				Instantiate (letterPrefabs [k], positionMatrix [i].column [j].position, Quaternion.identity);
			}
		}
	}

	void GenerateInvaders ()
	{
		ArrayList charArrayList = new ArrayList ();
		charArrayList.AddRange (characterArray);
		prefabLookup = new Hashtable ();
		addPrefabLookupEntries (charArrayList, letterPrefabs);
	}

	void addPrefabLookupEntries (ArrayList charArray, GameObject[] letterPrefabs)
	{
		for (int i = 0; i < charArray.Count; i++) {
			Invader letter = new Invader ();
			letter.ColumnIndex = (int)(i % 5);
			letter.character = (char)charArray [i];
			letter.Prefab = letterPrefabs [i];
			prefabLookup.Add (charArray [i].ToString (), letter);

		}
	}

	void spawmInvadersForLevel6 ()
	{
		string targetWord = commonWords [currentSubLevel - 1];
		int wordCount = targetWord.Length - 1;
		for (int i = 0; i <= wordCount; i++) {
			Invader invader = (Invader)prefabLookup [targetWord [i].ToString ()];
			targetWordText.text = targetWord;
			GameObject  invaderGameObject = (GameObject)Instantiate (invader.Prefab, positionMatrix [wordCount - i].column [invader.ColumnIndex].position, Quaternion.identity);
			GameObject colredBar = GetComponent<AttachColoredBar>().attachColoredBarForInvader(invaderGameObject, invader.character);
			colredBar.transform.parent = invaderGameObject.transform;
		}
	}

	void spawmInvadersForLevel7 ()
	{
		for (int i = 0; i < 4; i++) {

			Instantiate (numberPrefabs [i], positionMatrix [i].column [0].position, Quaternion.identity);
		}
	}

	void spawmInvadersForLevel8 ()
	{
		for (int i = 0; i < 4; i++) {
			
			Instantiate (numberPrefabs [i + 4], positionMatrix [i].column [1].position, Quaternion.identity);
		}
	}

	void spawmInvadersForLevel9 ()
	{
		for (int i = 0; i < 2; i++) {
			Instantiate (numberPrefabs [i + 8], positionMatrix [i].column [2].position, Quaternion.identity);
		}
	}

	void spawmInvadersForLevel10 ()
	{
		for (int i = 0; i < 4; i++) {

			Instantiate (punctuationPrefabs [i], positionMatrix [i].column [4].position, Quaternion.identity);
		}
		Instantiate (punctuationPrefabs [4], positionMatrix [0].column [3].position, Quaternion.identity);
	}

	void spawmInvadersForLevel11 ()
	{
		Instantiate (symbolPrefabsFirstRow [0], positionMatrix [0].column [0].position, Quaternion.identity); // &
		Instantiate (symbolPrefabsFirstRow [1], positionMatrix [0].column [1].position, Quaternion.identity); // $
		Instantiate (symbolPrefabsFirstRow [2], positionMatrix [0].column [2].position, Quaternion.identity); // #
		Instantiate (symbolPrefabsFirstRow [3], positionMatrix [0].column [3].position, Quaternion.identity); // @
		Instantiate (symbolPrefabsFirstRow [4], positionMatrix [0].column [4].position, Quaternion.identity); // _
		Instantiate (symbolPrefabsSecondRow [0], positionMatrix [1].column [0].position, Quaternion.identity); // )
		Instantiate (symbolPrefabsSecondRow [1], positionMatrix [1].column [2].position, Quaternion.identity); // +
		Instantiate (symbolPrefabsSecondRow [2], positionMatrix [1].column [3].position, Quaternion.identity); // =
		Instantiate (symbolPrefabsThirdRow [0], positionMatrix [2].column [0].position, Quaternion.identity); // euro
		Instantiate (symbolPrefabsThirdRow [1], positionMatrix [2].column [2].position, Quaternion.identity); // (
		Instantiate (symbolPrefabsThirdRow [2], positionMatrix [2].column [3].position, Quaternion.identity); // *
		Instantiate (symbolPrefabsFourthRow [0], positionMatrix [3].column [2].position, Quaternion.identity); // -
		Instantiate (symbolPrefabsFourthRow [1], positionMatrix [3].column [3].position, Quaternion.identity); // %
//		Instantiate (symbolPrefabsFifthRow [0], positionMatrix [4].column [3].position, Quaternion.identity); // /

	}
	#region old methods
//	void instantiateInvaders (int invaderColumn, int invaderRow)
//	{
//		
//		switch (invaderColumn) {
//			
//		case (1):
//			Instantiate (firstColumnInvaders [generateRandomIndex ()], firstColoumSpawnPositions [invaderRow].position, Quaternion.identity);
//			break;
//		case (2):
//			Instantiate (secondColumnInvaders [generateRandomIndex ()], secondColoumSpawnPositions [invaderRow].position, Quaternion.identity);
//			break;
//		case (3):
//			Instantiate (thirdColumnInvaders [generateRandomIndex ()], thirdColoumSpawnPositions [invaderRow].position, Quaternion.identity);
//			break;
//		case (4):
//			Instantiate (fourthColumnInvaders [generateRandomIndex ()], fouthColoumSpawnPositions [invaderRow].position, Quaternion.identity);
//			break;
//		case (5):
//			Instantiate (fifthColumnInvaders [generateRandomIndexForFifthColumn ()], fifthColoumSpawnPositions [invaderRow].position, Quaternion.identity);
//			break;
//		default :
//			break;
//			
//		}
//		
//	}
//	
//	int generateRandomIndex ()
//	{
//		if (currentLevel > 3) {
//			return Random.Range (0, 10);
//			
//		} else {
//			return Random.Range (0, 5);
//			
//		}
//	}
//	
//	int generateRandomIndexForFifthColumn ()
//	{
//		if (currentLevel > 3) {
//			return Random.Range (0, 8);
//			
//		} else {
//			return Random.Range (0, 4);
//			
//		}
//
//		
//	}

	#endregion




}