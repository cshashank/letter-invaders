﻿using UnityEngine;
using System.Collections;

public class bulletBehaviour : MonoBehaviour
{
	public float speed = 20.0f;
	public int colorCode;
	public string tileSeq;
	// Update is called once per frame
	void FixedUpdate ()
	{
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, speed);
		// Destroy the bullet if it crosses the screen height.
		Vector3 position = Camera.main.WorldToScreenPoint (transform.position);
		if (position.y > Screen.height ) {
			Destroy(gameObject);
		}

	}

}



