﻿using UnityEngine;
using System.Collections;

public class multiColoredBulletsLookup : MonoBehaviour
{
	public GameObject[] numberBullets;
	public GameObject[] punctuationBullets;
	public GameObject[] commonSymbolsBullets;
	public Hashtable multiColorBulletLookupTable;
	// Use this for initialization
	void Start ()
	{
		generateNumberBulletLookup ();
		generatePunctuationBulletLookup ();
		generateCommonSymbolsLookup ();
	}
	
	void generateNumberBulletLookup ()
	{
		// Generete a hashtable with keys as the tileSeq and values as bullets of numbers. 
		multiColorBulletLookupTable = new Hashtable ();

		multiColorBulletLookupTable.Add ("323", numberBullets [0]);
		multiColorBulletLookupTable.Add ("121", numberBullets [1]);
		multiColorBulletLookupTable.Add ("12321", numberBullets [2]);
		multiColorBulletLookupTable.Add ("1234321", numberBullets [3]);
		multiColorBulletLookupTable.Add ("123454321", numberBullets [4]);
		multiColorBulletLookupTable.Add ("212", numberBullets [5]);
		multiColorBulletLookupTable.Add ("232", numberBullets [6]);
		multiColorBulletLookupTable.Add ("23432", numberBullets [7]);
		multiColorBulletLookupTable.Add ("2345432", numberBullets [8]);
		multiColorBulletLookupTable.Add ("32123", numberBullets [9]);
	}
	// Generete a hashtable with keys as the tileSeq and values as bullets of punctuations.
	void generatePunctuationBulletLookup ()
	{

		multiColorBulletLookupTable.Add ("5434", punctuationBullets [0]);
		multiColorBulletLookupTable.Add ("54345", punctuationBullets [1]);
		multiColorBulletLookupTable.Add ("5432345", punctuationBullets [2]);
		multiColorBulletLookupTable.Add ("545", punctuationBullets [1]);
		multiColorBulletLookupTable.Add ("543212345", punctuationBullets [4]);
	}

	void generateCommonSymbolsLookup ()
	{

		multiColorBulletLookupTable.Add ("4321234", commonSymbolsBullets [0]);// =
		multiColorBulletLookupTable.Add ("343", commonSymbolsBullets [1]);// +
		multiColorBulletLookupTable.Add ("34543", commonSymbolsBullets [2]);// -
		multiColorBulletLookupTable.Add ("2123", commonSymbolsBullets [3]);// (
		multiColorBulletLookupTable.Add ("2321", commonSymbolsBullets [4]);// )
		multiColorBulletLookupTable.Add ("454", commonSymbolsBullets [5]);// %
		multiColorBulletLookupTable.Add ("43234", commonSymbolsBullets [6]);// *
		multiColorBulletLookupTable.Add ("54321234", commonSymbolsBullets [7]);// pounds
		multiColorBulletLookupTable.Add ("23454321", commonSymbolsBullets [8]);// euro
		multiColorBulletLookupTable.Add ("45432", commonSymbolsBullets [9]);// $
		multiColorBulletLookupTable.Add ("34323", commonSymbolsBullets [10]);// #
		multiColorBulletLookupTable.Add ("23454", commonSymbolsBullets [11]);// @
		multiColorBulletLookupTable.Add ("434", commonSymbolsBullets [12]);// /
		multiColorBulletLookupTable.Add ("5432123", commonSymbolsBullets [13]);// "
		multiColorBulletLookupTable.Add ("454321", commonSymbolsBullets [14]);// &

	}


}
