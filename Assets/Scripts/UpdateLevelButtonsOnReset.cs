﻿using UnityEngine;
using System.Collections;

public class UpdateLevelButtonsOnReset : MonoBehaviour {

	static int noOfLevels = 11;

	public void updateButtonsOnReset()
	{
		for(int i = 0; i < noOfLevels; i++) {

			GameObject.Find("Level"+(i+1).ToString()).GetComponent<CheckClearedLevels>().setupButton();

		}
	}

}
