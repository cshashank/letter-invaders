﻿using UnityEngine;
using System.Collections;

public class LevelManagerScript : MonoBehaviour
{

	public	int currentLevel;
	public int currentSubLevel;
	public int clearedLevels;
	
	void Awake ()
	{
		DontDestroyOnLoad (this);
		if (Time.time == 0) {
			PlayerPrefs.GetInt ("currentLevel", 1);
			PlayerPrefs.GetInt ("currentSubLevel", 1);
			PlayerPrefs.GetInt ("ClearedLevels", 1);
			PlayerPrefs.Save ();
		}
		currentLevel = PlayerPrefs.GetInt ("currentLevel");
		currentSubLevel = PlayerPrefs.GetInt ("currentSubLevel");
		clearedLevels = PlayerPrefs.GetInt ("ClearedLevels");
		Debug.Log (currentLevel);
	}

	public void IncreaseLevel ()
	{
		currentLevel += 1; 
		PlayerPrefs.SetInt ("currentLevel", currentLevel);
		if (currentLevel > clearedLevels) {
			PlayerPrefs.SetInt ("ClearedLevels", currentLevel);
		}

		PlayerPrefs.Save ();
	}

	public void ResetLevel ()
	{
	
		currentLevel = 1;
		PlayerPrefs.SetInt ("currentLevel", currentLevel);
		PlayerPrefs.Save ();
	}

	public void IncreaseSubLevel ()
	{
		currentSubLevel += 1; 
		Debug.Log (currentSubLevel);
		PlayerPrefs.SetInt ("currentSubLevel", currentSubLevel);
		PlayerPrefs.Save ();
		if (currentLevel == 6 && currentSubLevel == 8) {
			subLevelsCompleted ();

		} else if (currentLevel > 6 && currentSubLevel == 3) {
			subLevelsCompleted ();
		} else {
			Application.LoadLevel (1);
	
		}
	}

	public void ResetSubLevel ()
	{
		currentSubLevel = 1;
		PlayerPrefs.SetInt ("currentSubLevel", currentSubLevel);
		PlayerPrefs.Save ();
	}

	public void subLevelsCompleted ()
	{
		IncreaseLevel ();
		ResetSubLevel ();
		Application.LoadLevel (2);
	}
}
