﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
	public LevelManagerScript levelManager;
	public Canvas menu; 
	private bool isShowing;

	void Start(){

		isShowing = false;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{

		if (coll.collider.gameObject.layer == LayerMask.NameToLayer ("Invaders layer")) {

			// This is called when the monsters collide with tiles. No need to Reset Levels now. So commenting that line of code. 
//			levelManager.ResetLevel();
			isShowing = true;
			Time.timeScale = 0;

		}
	}

	void Update() {

		menu.enabled = isShowing;

	}

	void restartGame ()
	{
		
		Time.timeScale = 1;
		Application.LoadLevel (0);
	}


}
