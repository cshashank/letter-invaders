﻿using UnityEngine;
using System.Collections;


public class ShowResetPopup : MonoBehaviour {
	public Canvas resetMenu;
	private bool isMenuShowing;


	void Start(){
		
		isMenuShowing = false;
	}

	void Update() {
		
		resetMenu.enabled = isMenuShowing;
		
	}


	public void hideResetMenu (){
	
		isMenuShowing = false;
	}

	public void showResetMenu () {

		isMenuShowing = true;
	}
}
