﻿using UnityEngine;
using System.Collections;

public class ClearAllPlayerPrefs : MonoBehaviour {

 public	void OnClick () {
	
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.SetInt ("currentLevel", 1);
		PlayerPrefs.SetInt ("ClearedLevels", 1);
		PlayerPrefs.Save ();

	}

}
