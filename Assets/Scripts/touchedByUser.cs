﻿using UnityEngine;
using System.Collections;

public class touchedByUser : MonoBehaviour
{
	// A array that holds the prefabs of all the bullets
	public GameObject[] bulletPrefabs;
	public multiColoredBulletsLookup numberBullets;
	Bounds tilesBounds;
	GameObject bullet;
	TILE currentTile;
	string horizontalTileSeq;
	ArrayList tileSeqList;
	float camHalfHeight;

	enum TILE : int
	{
		BLUE_TILE=1,
		GREEN_TILE,
		YELLOW_TILE,
		ORANGE_TILE,
		RED_TILE,
		NO_TILE}
	;

	void Start ()
	{
		Collider2D TilesCollider2D = gameObject.GetComponent<Collider2D> ();
		tilesBounds = TilesCollider2D.bounds;
		camHalfHeight = Camera.main.orthographicSize;

	}

	void OnMouseUp ()
	{
		if (bullet != null) {

			bullet.GetComponent<bulletBehaviour> ().speed = 10.0f;
			bullet.GetComponent<bulletBehaviour>().tileSeq = horizontalTileSeq;
		}
	
//		Debug.Log (horizontalTileSeq);
		tileSeqList = null;
	}

	void OnMouseDrag ()
	{
		// Move all this code into a seperate method
		Vector3 position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		position.z = -1;
		Vector3 bulletSpawnAboveTiles = position;
		float tileWidth = tilesBounds.size.x / 5;
		currentTile = findTileAtTouch (position);
		bulletSpawnAboveTiles.x = tileWidth * (currentTile - TILE.YELLOW_TILE);
		processTouchOnTile (currentTile);
		if (bullet != null) {
			bulletSpawnAboveTiles.y = (- camHalfHeight) + tileWidth + (1.0f * bullet.GetComponent<Collider2D> ().bounds.size.y);
			bullet.transform.position = bulletSpawnAboveTiles;
			if (numberBullets.multiColorBulletLookupTable.ContainsKey(horizontalTileSeq)) {
				Destroy(bullet);
				GameObject coloredNumberBullet = (GameObject)numberBullets.multiColorBulletLookupTable [horizontalTileSeq];
				bullet = (GameObject)Instantiate (coloredNumberBullet, bulletSpawnAboveTiles, transform.rotation);
			}

		}


	}

	void OnMouseDown ()
	{
			// Move all this code into a seperate method
			Vector3 position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			position.z = -1;
			Vector3 bulletSpawnAboveTiles = position;
			float tileWidth = tilesBounds.size.x / 5;

			currentTile = findTileAtTouch (position);
			bulletSpawnAboveTiles.x = tileWidth * (currentTile - TILE.YELLOW_TILE);
			bullet = (GameObject)Instantiate (bulletPrefabs [((int)currentTile - 1)], bulletSpawnAboveTiles, transform.rotation);
			bulletSpawnAboveTiles.y = (- camHalfHeight) + tileWidth + (1.0f * bullet.GetComponent<Collider2D> ().bounds.size.y);
			processTouchOnTile (currentTile);

			if (bullet != null) {

				bullet.GetComponent<bulletBehaviour> ().colorCode = (int)currentTile;
			}
	
	}

	TILE findTileAtTouch (Vector3 position)
	{
		TILE tile = TILE.NO_TILE;
		// The center of tiles lies at the center of yellow tile. Shift the tiles to right by half the width of tiles to 
		// to make blue tile left edge's position as 0.0.
		float adjustedPosition = (tilesBounds.size.x / 2) + position.x; 
		float tileWidth = tilesBounds.size.x / 5;
		if (adjustedPosition >= 0.0 && adjustedPosition <= tileWidth)
			tile = TILE.BLUE_TILE;
		else if (adjustedPosition >= tileWidth && adjustedPosition <= tileWidth * 2)
			tile = TILE.GREEN_TILE;
		else if (adjustedPosition >= (tileWidth * 2) && adjustedPosition <= tileWidth * 3)
			tile = TILE.YELLOW_TILE;
		else if (adjustedPosition >= (tileWidth * 3) && adjustedPosition <= tileWidth * 4)
			tile = TILE.ORANGE_TILE;
		else if (adjustedPosition >= (tileWidth * 4) && adjustedPosition <= tileWidth * 5)
			tile = TILE.RED_TILE;
		else
			tile = TILE.NO_TILE; 
		return tile;
	}

	
	// The below code is required for generating tile seq. But now matching the color instead of tileseq

void processTouchOnTile(TILE newTile)
	{
		//List is empty therefore it is the first tile.
		if(tileSeqList == null) {
			addTileIntoTileSeq(newTile);
		}
		else{
			//find out previous tile.
//			TILE prevTile = [[self.tileSeqList objectAtIndex:self.tileSeqList.count - 1] intValue];
			TILE prevTile = (TILE)tileSeqList[tileSeqList.Count - 1];

			//touch direction is from left to right.
			if (prevTile < newTile){
				//insert missing tiles.
//				[self addTilesIntoTileSeqInBetween:prevTile toTile:newTile];
				addReverseTilesIntoTileSeqInBetween(prevTile, newTile);
			}
			//touch direction is from right to left.
			else if (prevTile > newTile){
				//insert missing tiles.
//				[self addReverseTilesIntoTileSeqInBetween:prevTile toTile:newTile];
				addReverseTilesIntoTileSeqInBetween(prevTile, newTile);
			}
			//Add new tile if it is different from previous tile.
			if (prevTile != newTile){
				addTileIntoTileSeq((TILE)newTile);
			}
		}
	}

void addTileIntoTileSeq(TILE newTile)
	{
		if (tileSeqList == null){
			tileSeqList = new ArrayList();
			tileSeqList.Add((int)newTile);
			horizontalTileSeq = System.String.Empty;
//			[self.horizontalTileSeq appendString:[NSString stringWithFormat:@"%ld",(long)newTile]];
			horizontalTileSeq += ((int)newTile).ToString();
		}
		else{
			tileSeqList.Add((int)newTile);
//			[self.horizontalTileSeq appendString:[NSString stringWithFormat:@"%ld",(long)newTile]];
			horizontalTileSeq += ((int)newTile).ToString();
		}
	}
	
	/**
 This adds tiles into forward sequence between two tiles. For example if fromTile:2 and toTile:5 then
 the tile seq is 3,4.
 */
void addTilesIntoTileSeqInBetween(TILE fromTile, TILE toTile)
	{
		int totalinBetweenTiles = toTile - fromTile - 1;
		//there are tiles in between from and to tiles.
		if (totalinBetweenTiles > 0){
			for (int tile = (int)fromTile + 1; tile < (int)toTile; tile++){
				addTileIntoTileSeq((TILE)tile);
			}
		}
	}
	/**
 This adds tiles into reverse sequence between two tiles. For example if fromTile:5 and toTile:2 then
 the tile seq is 4,3
 */
void addReverseTilesIntoTileSeqInBetween(TILE fromTile, TILE toTile)
	{
		int totalinBetweenTiles = fromTile - toTile - 1;
		//there are tiles in between from and to tiles.
		if (totalinBetweenTiles > 0){
			for (int tile = (int)fromTile - 1; tile > (int)toTile; tile--){
				addTileIntoTileSeq((TILE)tile);
			}
		}
	}


}
