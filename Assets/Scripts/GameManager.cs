﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	static int currentLevel;
	public LevelManagerScript levelManager;

	// Update is called once per frame
	void Update ()
	{
		currentLevel = levelManager.currentLevel;
		// If all the invaders are killed, load the level (score scene).
		if (GameObject.FindGameObjectsWithTag ("invader").Length == 0) {
			if (currentLevel < 6) {
				levelManager.IncreaseLevel ();
				Application.LoadLevel (2);

			} else if (currentLevel < 12) {
				levelManager.IncreaseSubLevel ();
	
			}
		}
	}

	public void restartGame ()
	{
		Application.LoadLevel (0);
		Time.timeScale = 1;
	}

	public void QuitGame ()
	{
		//Quit is ignored in the editor or the web player
		Application.Quit ();
	}
}
