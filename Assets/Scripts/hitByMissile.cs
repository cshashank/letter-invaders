﻿using UnityEngine;
using System.Collections;

public class hitByMissile : MonoBehaviour {

	public int colorCode;
	public string tileSeq; 

	void OnTriggerEnter2D(Collider2D other){

		if (other.GetComponent<bulletBehaviour> ().tileSeq == tileSeq) {
			Destroy (gameObject);
			Destroy (other.gameObject);
		} else {

			gameObject.GetComponent<Rigidbody2D>().gravityScale += 0.005f ;
			Destroy (other.gameObject);
		}
	}
}
