﻿using UnityEngine;
using System.Collections;

public class Invader
{

	private int invaderColumn;
	private GameObject invaderPrefab;
	private char invaderCharacter;

	public int ColumnIndex
	{
		get
		{
			return invaderColumn;
		}
		set
		{
			invaderColumn = value;
		}
	}
	
	public GameObject Prefab
	{
		get
		{
			return invaderPrefab;
		}
		set
		{
			invaderPrefab = value;
		}
	}

	public char character
	{
		get
		{
			return invaderCharacter;
		}
		set
		{
			invaderCharacter = value;
		}
	}

}
