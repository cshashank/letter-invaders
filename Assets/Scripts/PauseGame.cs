﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour
{
	public Canvas instructionsMenu;
	public Canvas gameOverMenu;
	void OnMouseDown ()
	{
		if (gameOverMenu.enabled == false && instructionsMenu.enabled == false) {
//			if (Time.timeScale == 0)
//				Time.timeScale = 1;
//			else
			Time.timeScale = 0;
			instructionsMenu.GetComponent<InstructionMenuHandler> ().isMenuShowing = true;
		} else
			Time.timeScale = 1;
	}
}
