﻿using UnityEngine;
using System.Collections;

public class AttachColoredBar : MonoBehaviour
{
	public GameObject redBar;
	public GameObject blueBar;
	public GameObject orangeBar;
	public GameObject yellowBar;
	public GameObject greenBar;
	public GameObject greyBar;


	public GameObject attachColoredBarForInvader(GameObject invader, char invaderCharacter)
	{
		GameObject coloredBarToBeAttached = returnBarToBeShownForInvader (invaderCharacter);
		return	(GameObject)Instantiate(coloredBarToBeAttached, coloredBarSpawnPosition(invader), Quaternion.identity );
	}

	private Vector3 coloredBarSpawnPosition(GameObject invader){

		Vector3 barPosition = invader.transform.position;
		barPosition.y += invader.GetComponent<Renderer> ().bounds.size.z *2 + 0.1f; 
		barPosition.x = 0;
		return barPosition;
	}

	private GameObject returnBarToBeShownForInvader (char invaderCharacter)
	{

		switch (invaderCharacter) {
		case ('a'):
		case ('b'):
		case ('c'):
		case ('d'):
		case ('e'):
			return blueBar;

		case ('f'):
		case ('g'):
		case ('h'):
		case ('i'):
		case ('j'):
			return greenBar;

		case ('k'):
		case ('l'):
		case ('m'):
		case ('n'):
		case ('o'):
			return yellowBar;

		case ('p'):
		case ('q'):
		case ('r'):
		case ('s'):
		case ('t'):
			return orangeBar;


		case ('v'):
		case ('w'):
		case ('x'):
		case ('y'):
		case ('z'):
			return redBar;

		}
		return null;
	}




}
